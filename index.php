<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Aufgabe 3 - Directory Stats</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>
    </head>
    <body>
        <?php
            include 'config.php';   

            $newestFile = "";
            $newestFileDate = 0;

            $oldestFile = "";
            $oldestFileDate = time(new DateTime('9999-01-01 00:00'));

            $fileCount = 0;
            $directoryCount = 0;


            function recursive_scan($path) {
                global $fileCount, $directoryCount, $newestFile, $newestFileDate, $oldestFile, $oldestFileDate;

                $path = rtrim($path, '/');
                if(is_dir($path)) {
                    $directoryCount = $directoryCount + 1;

                    $files = scandir($path);
                    foreach($files as $file) if($file != '.' && $file != '..') recursive_scan($path . '/' . $file);
                }
                else {
                    $fileCount = $fileCount + 1;

                    $lastModified = filemtime($path);

                    if ($lastModified > $newestFileDate) {
                        $newestFile = $path;
                        $newestFileDate = $lastModified;
                    }

                    if ($lastModified < $oldestFileDate) {
                        $oldestFile = $path;
                        $oldestFileDate = $lastModified;
                    }
                }
            }

            recursive_scan($folderToScan);
        ?>

        <div class="row">
            <div class="col s12 m6 l4">
            <div class="card blue-grey lighten-1">
                <div class="card-content white-text">
                <span class="card-title">Newest File</span>
                <p><?php echo $newestFile ?> (<?php echo date("'Y-m-d H:i:s'", $newestFileDate) ?>)</p>
                </div>
            </div>
            </div>

            <div class="col s12 m6 l4">
            <div class="card blue-grey lighten-1">
                <div class="card-content white-text">
                <span class="card-title">Oldest File</span>
                <p><?php echo $oldestFile ?> (<?php echo date("'Y-m-d H:i:s'", $oldestFileDate) ?>)</p>
                </div>
            </div>
            </div>

            <div class="col s12 m6 l4">
            <div class="card blue-grey lighten-1">
                <div class="card-content white-text">
                <span class="card-title">File Count</span>
                <p><?php echo $fileCount ?> Files</p>
                </div>
            </div>
            </div>

            <div class="col s12 m6 l4">
            <div class="card blue-grey lighten-1">
                <div class="card-content white-text">
                <span class="card-title">Directory Count</span>
                <p><?php echo $directoryCount ?> Folders</p>
                </div>
            </div>
            </div>
        </div>

    </body>
</html>